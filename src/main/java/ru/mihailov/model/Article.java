package ru.mihailov.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="article")
public class Article {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name="title")
    private String title;

    @Column(name="category")
    private String category;

}
