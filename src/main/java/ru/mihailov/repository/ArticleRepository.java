package ru.mihailov.repository;

import org.springframework.data.repository.CrudRepository;
import ru.mihailov.model.Article;

public interface ArticleRepository extends CrudRepository<Article, String> {

}
