package ru.mihailov.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mihailov.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(String username);

}
