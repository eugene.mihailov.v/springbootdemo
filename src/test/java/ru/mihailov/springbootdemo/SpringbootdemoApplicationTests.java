package ru.mihailov.springbootdemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "ru.mihailov"})
@SpringBootTest
class SpringbootdemoApplicationTests {

	@Test
	void contextLoads() {
	}

}
